/*autor: mabel flores quispe.*/
package ejercicio1;
import javax.swing.JOptionPane;
import java.text.DecimalFormat;
public class ejercicio8 {
    public static void main(String[] args) {
        String InputText=JOptionPane.showInputDialog("Digite la cantica a convertir:");
        double Bs = Double.parseDouble(InputText);
        String din=JOptionPane.showInputDialog("A que desea convertir?:");
        JOptionPane.showMessageDialog(null,convertir(Bs,din));
    }
    public static String convertir(double Bs,String din){
        double res=0;
        switch(din){
            case "dolares":
                res=Bs*0.14451;
                break;
            case "euros": 
                res=Bs*0.12930;
                break;
            case "libras":
                res=Bs*0.11552;
                break;
            default:
                JOptionPane.showMessageDialog(null,"ERROR:");
                break;
        }
        DecimalFormat df=new DecimalFormat("#.00");
        return "la cantidad en:"+din + "es"+df.format(res);
    }
}
