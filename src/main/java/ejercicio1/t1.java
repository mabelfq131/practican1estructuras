/*autor: mabel flores quispe.*/
package ejercicio1;
import javax.swing.JOptionPane;
public class t1 {
    public static void main(String[] args) {
        int result=0;
        String typeText= "";
        String typeinput=JOptionPane.showInputDialog("introduzca la operacion a realizar");
        switch( typeinput){
            case "suma":
                typeText=JOptionPane.showInputDialog("introduzca el primer numero");
                int x=Integer.parseInt(typeText);
                typeText=JOptionPane.showInputDialog("introduzca el segundo numero");
                int y=Integer.parseInt(typeText);
                result=suma(x,y);
                JOptionPane.showMessageDialog(null,"la suma es:"+result);
                break;
            case "resta":
                typeText=JOptionPane.showInputDialog("introduzca el primer numero");
                int r1=Integer.parseInt(typeText);
                typeText=JOptionPane.showInputDialog("introduzca el segundo numero");
                int r2=Integer.parseInt(typeText);
                result=resta(r1,r2);
                JOptionPane.showMessageDialog(null,"la resta es:"+result);
                break;
            case "multiplicacion":
                typeText=JOptionPane.showInputDialog("introduzca el primer numero");
                int m1=Integer.parseInt(typeText);
                typeText=JOptionPane.showInputDialog("introduzca el segundo numero");
                int m2=Integer.parseInt(typeText);
                result=multiplicacion(m1,m2);
                JOptionPane.showMessageDialog(null,"la multiplicacion es:"+result);
                break;
            case "division":
                typeText=JOptionPane.showInputDialog("introduzca el primer numero");
                int d1=Integer.parseInt(typeText);
                typeText=JOptionPane.showInputDialog("introduzca el segundo numero");
                int d2=Integer.parseInt(typeText);
                result=division(d1,d2);
                JOptionPane.showMessageDialog(null,"la division es:"+result);
                break;
            default:
                JOptionPane.showMessageDialog(null,"por favor elija una operacion");
                break;
        }
    }
    public static int suma(int a, int b){
        return a+b;
    }
    public static int resta (int a, int b){
        return a-b;
    }
    public static int multiplicacion(int a, int b){
        return a*b;
    }
    public static int division (int a, int b){
        return a/b;
    }
}
    

