/*autor: mabel flores quispe.*/
package ejercicio1;

import javax.swing.JOptionPane;
public class ejercicio10 {
    public static void main(String[] args) {
        String typeInput=JOptionPane.showInputDialog("introduzca un numero");
        int a = Integer.parseInt(typeInput);
        cifras(a);
    }
    static void cifras(int a)
    {
        String cad = ""; 
        String res = "";
        if(esTresCifras(a))
        {
            while(a!=0)
            {
                 cad+=a%10+" ";
                 a=a/10;
            }
        }
        for(int i = cad.length()-1;i>=0;i--)
        {
            res+=cad.charAt(i);
        }
        JOptionPane.showMessageDialog(null,res);
        /*System.out.println(res);*/
    }
    static boolean esTresCifras(int a)
    {
        int cant=0;
        while(a!=0)
        {
           cant++;
           a=a/10;
        }
        return cant==3;
    }
    
}
