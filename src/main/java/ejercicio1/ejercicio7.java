/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio1;
/*autor: mabel flores quispe.*/
import javax.swing.JOptionPane;
public class ejercicio7 {
    public static void main(String[] args) {
        try {
            String InputText=JOptionPane.showInputDialog("introduzca un numero:");
            int num = Integer.parseInt(InputText);
            JOptionPane.showMessageDialog(null,"la cantidad de cifras es:"+cantCifras(num));
        } catch (Exception e) {
             JOptionPane.showMessageDialog(null,"el dato ingresado no es positivo");
        }
    }
    public static int cantCifras(int num)
    {
        int cant=0;
        while(num!=0)
        {
           cant++;
           num=num/10;
        }
        return cant;
    }
    
}
